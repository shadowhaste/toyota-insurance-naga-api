<?php

return [
    "insurance" => [
        "create" => [
            "200" => "Insurance created successfully."
        ],
        "has_active" => "There is still an active insurance exist for this vehicle."
    ],
    "payment" => [
        "cannot" => "Cannot do payment here. Please pay the first unpaid amortisation."
    ],
    "sms" => [
        "reminder" => [
            "whole" => "Dear CUSTOMER_NAME, \n Your vehicle model VEHICLE_MODEL insurance INSURANCE_REFERENCE will be expiring on EXPIRATION_DATE. Please renew. Thanks",
            "half" => "Dear CUSTOMER_NAME, \n Your vehicle model VEHICLE_MODEL insurance INSURANCE_REFERENCE will be expiring on EXPIRATION_DATE. Please renew. Thanks"
        ]
    ]
];
