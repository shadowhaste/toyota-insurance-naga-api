<?php

class AdminTest extends TestCase {

//    public function testRegisterDefaultAdmin() {
//        $response = $this->post("/register", []);
//        $response->assertResponseStatus(422);
//        $response->seeJsonStructure([
//            'username',
//            'password',
//        ]);
//
//        $response = $this->post("/register", [
//            'username' => "admin",
//            'password' => "test",
//        ]);
//        $response->assertResponseStatus(200);
//        $response->seeJsonStructure([
//            'message'
//        ]);
//    }

    public function testRegisterAdmin() {
        $response = $this->post("/register", []);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'username',
            'password',
        ]);

        $testdata = factory(\App\Models\Admin::class)->make();
        $response = $this->post("/register", [
            'username' => $testdata->username,
            'password' => $testdata->password,
        ]);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testLoginAdmin() {
        //register
        $testdata = factory(\App\Models\Admin::class)->make();
        $response = $this->post("/register", [
            'username' => $testdata->username,
            'password' => $testdata->password,
        ]);

        //login
        //test 422
        $response = $this->post("/login", []);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'username',
            'password'
        ]);

        //test 200
        $response = $this->post("/login", [
            'username' => $testdata->username,
            'password' => $testdata->password
        ]);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'user'
        ]);
    }

    //customer
    public function testCustomerCreate(){

        // TEST 403
        $response = $this->post("customer/add", []);
        $response->assertResponseStatus(403);

        $user = factory(\App\Models\Admin::class)->make();
        //test 422
        $response = $this->post("/customer/add", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'email',
            'lastname',
            'firstname',
            'middlename',
            'mobile_number',
            'occupation'
        ]);


        //test 200
        $testdata = factory(\App\Models\Customer::class)->make();
        $response = $this->post("/customer/add", [
            'email' => $testdata->email,
            'tin' => $testdata->tin,
            'lastname' => $testdata->lastname,
            'firstname' => $testdata->firstname,
            'middlename' => $testdata->middlename,
            'suffix' => $testdata->suffix,
            'address' => $testdata->address,
            'phone_number' => $testdata->phone_number,
            'mobile_number' => $testdata->mobile_number,
            'facebook_id' => $testdata->facebook_id,
            'birth_date' => $testdata->birth_date,
            'gender' => $testdata->gender,
            'civil_status' => $testdata->civil_status,
            'height' => $testdata->height,
            'weight' => $testdata->weight,

            'occupation' => $testdata->occupation,
            'current_employer' => $testdata->current_employer,
            'current_employer_address' => $testdata->current_employer_address,
            'industry' => $testdata->industry,
            'office_phone_number' => $testdata->office_phone_number,
            'email_or_website' => $testdata->email_or_website,

            'spouse_lastname' => $testdata->spouse_lastname,
            'spouse_firstname' => $testdata->spouse_firstname,
            'spouse_middlename' => $testdata->spouse_middlename,
            'spouse_suffix' => $testdata->spouse_suffix,
            'spouse_mobile_number' => $testdata->spouse_mobile_number,
            'spouse_occupation' => $testdata->spouse_occupation,
            'spouse_current_employer' => $testdata->spouse_current_employer,
            'spouse_current_employer_address' => $testdata->spouse_current_employer_address,
            'spouse_industry' => $testdata->spouse_industry,
            'spouse_office_phone_number' => $testdata->spouse_office_phone_number,
            'spouse_email_or_website' => $testdata->spouse_email_or_website,
            'children' => $testdata->children
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testCustomerView(){

        $user = factory(\App\Models\Admin::class)->make();

        // TEST 403
        $response = $this->post("customer/view");
        $response->assertResponseStatus(403);

        // TEST 422
        $response = $this->post("customer/view", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'uuid',
        ]);

        // TEST 200
        $customerData = factory(\App\Models\Customer::class)->create();
        $response = $this->post("customer/view", [
            "uuid" => $customerData->uuid
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testCustomerList(){

        //test 403
        $response = $this->post("customer/list");
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);

        // TEST 200
        $user = factory(\App\Models\Admin::class)->make();

        factory(\App\Models\Customer::class, 10)->create([
            "firstname" => "Juan" . random_int(1, 2000),
            "lastname" => "Tamad" . random_int(1, 2000)
        ]);

        $response = $this->post("customer/list", [
            "keyword" => "Juan",
            "dateFrom" => \Carbon\Carbon::now()->format('Y-m-d'),
            "dateTo" => \Carbon\Carbon::now()->addDay()->format('Y-m-d')
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "list" => [
                "*" => [
                ]
            ],
            "max_page",
            "next_page",
            "prev_page",
        ]);

    }

    //vehicle
    public function testAddVehicle(){
        // TEST 403
        $response = $this->post("vehicle/add", []);
        $response->assertResponseStatus(403);

        $user = factory(\App\Models\Admin::class)->make();
        //test 422
        $response = $this->post("vehicle/add", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'customer_uuid',
            'model_no',
            'make',
            'color',
            'cs_no',
            'vin',
            'vsi',
            'engine',
            'release_date'
        ]);

        //test 200
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->make();
        $response = $this->post("/vehicle/add", [
            'customer_uuid' => $customerData->uuid,
            'model_no' => $vehicleData->model_no,
            'make' => $vehicleData->make,
            'description' => $vehicleData->description,
            'color' => $vehicleData->color,
            'cs_no' => $vehicleData->cs_no,
            'vin' => $vehicleData->vin,
            'vsi' => $vehicleData->vsi,
            'engine' => $vehicleData->engine,
            'displacement' => $vehicleData->displacement,
            'year_model' => $vehicleData->year_model,
            'gross_weight' => $vehicleData->gross_weight,
            'key_number' => $vehicleData->key_number,
            'release_date' =>  \Carbon\Carbon::now()->format('Y-m-d'),
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testVehicleList(){
        //test 403
        $response = $this->post("vehicle/list");
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);

        // TEST 200
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();

        factory(\App\Models\Vehicle::class, 10)->create([
            "customer_uuid" => $customerData->uuid,
            "customer_id" => $customerData->id,
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
        ]);

        $response = $this->post("vehicle/list", [
            "customer_uuid" => $customerData->uuid,
            "keyword" => "",
            "dateFrom" => \Carbon\Carbon::now()->format('Y-m-d'),
            "dateTo" => \Carbon\Carbon::now()->addDay()->format('Y-m-d')
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "list" => [
                "*" => [
                ]
            ],
            "max_page",
            "next_page",
            "prev_page",
        ]);

    }

    public function testVehicleView(){

        $user = factory(\App\Models\Admin::class)->make();

        // TEST 403
        $response = $this->post("vehicle/view");
        $response->assertResponseStatus(403);

        // TEST 422
        $response = $this->post("vehicle/view", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'uuid',
        ]);

        // TEST 200
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id
        ]);

        $response = $this->post("vehicle/view", [
            "uuid" => $vehicleData->uuid
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    //insurance
    private function getFinalizedInsurance(){
        //get unpaid amort first
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id
        ]);

        $insuranceData = factory(\App\Models\Insurance::class)->make();
        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 18700,
                    'amount_without_aon' => 18700,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 200000,
                    'amount_with_aon' => 420,
                    'amount_without_aon' => 420,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 4250,
                    'amount_without_aon' => 0,
                ],
            ]
        ], $this->AdminHeader($user));

        $insuranceQuote = $this->decode($response)->model;

        //test 200
        $response = $this->post("insurance/finalize", [
            'uuid' => $insuranceQuote->uuid,
            'registration_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'initial_payment' => 8000,
            'has_availed' => 'with_aon',
            'provision_no' => str_random(10)
        ], $this->AdminHeader($user));
        return $this->decode($response)->model;
    }

    public function testAddInsuranceQuote(){
        // TEST 403
        $response = $this->post("insurance/add", []);
        $response->assertResponseStatus(403);

        $user = factory(\App\Models\Admin::class)->make();

        //test 422
        $response = $this->post("insurance/add", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'provider',
            'reference_id',
            'vehicle_uuid',
            'type'
        ]);

        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id
        ]);

        $insuranceData = factory(\App\Models\Insurance::class)->make();
        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => random_int(1000, 5000),
                    'amount_with_aon' => random_int(1000, 5000),
                    'amount_without_aon' => random_int(1000, 5000),
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => random_int(1000, 5000),
                    'amount_with_aon' => random_int(1000, 5000),
                    'amount_without_aon' => random_int(1000, 5000),
                ],
            ]
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
        ]);
    }

    public function testFinalizedInsuranceQuote(){
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id
        ]);

        $insuranceData = factory(\App\Models\Insurance::class)->make();
        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 18700,
                    'amount_without_aon' => 18700,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 200000,
                    'amount_with_aon' => 420,
                    'amount_without_aon' => 420,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 4250,
                    'amount_without_aon' => 0,
                ],
            ]
        ], $this->AdminHeader($user));

        $insuranceQuote = $this->decode($response)->model;
        // TEST 403
        $response = $this->post("insurance/finalize", []);
        $response->assertResponseStatus(403);

        //test 422
        $response = $this->post("insurance/finalize", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'uuid',
            'registration_date',
            'initial_payment',
            'has_availed',
        ]);

        //test 200
        $response = $this->post("insurance/finalize", [
            'uuid' => $insuranceQuote->uuid,
            'registration_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'initial_payment' => 8000,
            'has_availed' => 'with_aon',
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
        ]);

    }

    public function testViewInsurance(){

        //add insurance first
        $user = factory(\App\Models\Admin::class)->make();

        $insurance = $this->getFinalizedInsurance();

        // TEST 403
        $response = $this->post("insurance/view");
        $response->assertResponseStatus(403);

        // TEST 422
        $response = $this->post("insurance/view", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'uuid',
        ]);

        // TEST 200
        $response = $this->post("insurance/view", [
            "uuid" => $insurance->uuid
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testInsuranceList(){

        // TEST 200
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            "customer_uuid" => $customerData->uuid,
            'customer_id' => $customerData->id,
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
        ]);

        //add insurance
        $insuranceData = factory(\App\Models\Insurance::class)->make();

        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_id' => $vehicleData->id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => random_int(1000, 5000),
                    'amount_with_aon' => random_int(1000, 5000),
                    'amount_without_aon' => random_int(1000, 5000),
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => random_int(1000, 5000),
                    'amount_with_aon' => random_int(1000, 5000),
                    'amount_without_aon' => random_int(1000, 5000),
                ],
            ]
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);

        //test 403
        $response = $this->post("insurance/list");
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);

        //test 200
        $response = $this->post("insurance/list", [
            "vehicle_uuid" => $vehicleData->uuid,
            "keyword" => "",
            "dateFrom" => \Carbon\Carbon::now()->format('Y-m-d'),
            "dateTo" => \Carbon\Carbon::now()->addDay()->format('Y-m-d')
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "list" => [
                "*" => [
                ]
            ],
            "max_page",
            "next_page",
            "prev_page",
        ]);

    }

    //amortisations
    public function testUnpaidAmortisationList(){
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id
        ]);

        $insuranceData = factory(\App\Models\Insurance::class)->make();
        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 18700,
                    'amount_without_aon' => 18700,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 200000,
                    'amount_with_aon' => 420,
                    'amount_without_aon' => 420,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 4250,
                    'amount_without_aon' => 0,
                ],
            ]
        ], $this->AdminHeader($user));

        $insuranceQuote = $this->decode($response)->model;

        //test 200
        $response = $this->post("insurance/finalize", [
            'uuid' => $insuranceQuote->uuid,
            'registration_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'initial_payment' => 8000,
            'has_availed' => 'with_aon',
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);

        //test 403
        $response = $this->post("insurance/amortisation/unpaid/list");
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);

        //test 422
        $response = $this->post("insurance/amortisation/unpaid/list", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "uuid",
        ]);

        //test 200
        $response = $this->post("insurance/amortisation/unpaid/list", [
            'uuid' => $insuranceQuote->uuid,
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "list" => [
                "*" => [
                ]
            ],
        ]);

    }

    public function testViewAmortisation(){
        $user = factory(\App\Models\Admin::class)->make();
        // TEST 403
        $response = $this->post("insurance/amortisation/view", []);
        $response->assertResponseStatus(403);

        //test 422
        $response = $this->post("insurance/amortisation/view", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'uuid',
        ]);

        $insuranceQuote = $this->getFinalizedInsurance();
        //test 200
        $response = $this->post("insurance/amortisation/unpaid/list", [
            'uuid' => $insuranceQuote->uuid,
        ], $this->AdminHeader($user));
        $unpaidData = $this->decode($response)->list;

        $firstUnpaidAmortisation = $unpaidData[0];
        $response = $this->post("payment/add", [
            'amortisation_uuid' => $firstUnpaidAmortisation->uuid,
            'reference' => \App\Helpers\Helper::generateAutoTransactionNo(), //OR number
            'amount' => 16000,
            'payment_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        ], $this->AdminHeader($user));

        //test 422
        $response = $this->post("insurance/amortisation/view", [
            "uuid" => $firstUnpaidAmortisation->uuid
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
        ]);

    }

    //payments
    public function testAddPaymentWithAmountGreaterThanAmortisationAmount(){
        $user = factory(\App\Models\Admin::class)->make();

        // TEST 403
        $response = $this->post("payment/add", []);
        $response->assertResponseStatus(403);

        //test 422
        $response = $this->post("payment/add", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'reference',
            'amount',
            'payment_date',
        ]);


        //get unpaid amort first
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id
        ]);

        $insuranceData = factory(\App\Models\Insurance::class)->make();
        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 18700,
                    'amount_without_aon' => 18700,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 200000,
                    'amount_with_aon' => 420,
                    'amount_without_aon' => 420,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 4250,
                    'amount_without_aon' => 0,
                ],
            ]
        ], $this->AdminHeader($user));

        $insuranceQuote = $this->decode($response)->model;

        //test 200
        $response = $this->post("insurance/finalize", [
            'uuid' => $insuranceQuote->uuid,
            'registration_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'initial_payment' => 8000,
            'has_availed' => 'with_aon',
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);

        //test 403
        $response = $this->post("insurance/amortisation/unpaid/list");
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);

        //test 422
        $response = $this->post("insurance/amortisation/unpaid/list", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "uuid",
        ]);

        //test 200
        $response = $this->post("insurance/amortisation/unpaid/list", [
            'uuid' => $insuranceQuote->uuid,
        ], $this->AdminHeader($user));
        $unpaidData = $this->decode($response)->list;

        $firstUnpaidAmortisation = $unpaidData[0];
        $response = $this->post("payment/add", [
            'amortisation_uuid' => $firstUnpaidAmortisation->uuid,
            'reference' => \App\Helpers\Helper::generateAutoTransactionNo(), //OR number
            'amount' => 8000,
            'payment_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
           'message'
        ]);
    }

    public function testAddPaymentWithAmountDoubledAmortisationAmount(){
        $user = factory(\App\Models\Admin::class)->make();

        // TEST 403
        $response = $this->post("payment/add", []);
        $response->assertResponseStatus(403);

        //test 422
        $response = $this->post("payment/add", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'reference',
            'amount',
            'payment_date',
        ]);


        //get unpaid amort first
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id,
        ]);

        $insuranceData = factory(\App\Models\Insurance::class)->make();
        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 18700,
                    'amount_without_aon' => 18700,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 200000,
                    'amount_with_aon' => 420,
                    'amount_without_aon' => 420,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 4250,
                    'amount_without_aon' => 0,
                ],
            ]
        ], $this->AdminHeader($user));

        $insuranceQuote = $this->decode($response)->model;

        //test 200
        $response = $this->post("insurance/finalize", [
            'uuid' => $insuranceQuote->uuid,
            'registration_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'initial_payment' => 8000,
            'has_availed' => 'with_aon',
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);

        //test 403
        $response = $this->post("insurance/amortisation/unpaid/list");
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);

        //test 422
        $response = $this->post("insurance/amortisation/unpaid/list", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "uuid",
        ]);

        //test 200
        $response = $this->post("insurance/amortisation/unpaid/list", [
            'uuid' => $insuranceQuote->uuid,
        ], $this->AdminHeader($user));
        $unpaidData = $this->decode($response)->list;

        $firstUnpaidAmortisation = $unpaidData[0];
        $response = $this->post("payment/add", [
            'amortisation_uuid' => $firstUnpaidAmortisation->uuid,
            'reference' => \App\Helpers\Helper::generateAutoTransactionNo(), //OR number
            'amount' => 16000,
            'payment_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testAddPaymentWithFreeInsurance(){
        $user = factory(\App\Models\Admin::class)->make();

        // TEST 403
        $response = $this->post("payment/add", []);
        $response->assertResponseStatus(403);

        //test 422
        $response = $this->post("payment/add", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'reference',
            'amount',
            'payment_date',
        ]);


        //get unpaid amort first
        $user = factory(\App\Models\Admin::class)->make();
        $customerData = factory(\App\Models\Customer::class)->create();
        $vehicleData = factory(\App\Models\Vehicle::class)->create([
            'release_date' => \Carbon\Carbon::now()->format("Y-m-d"),
            'customer_uuid' => $customerData->uuid,
            'customer_id' => $customerData->id
        ]);

        $insuranceData = factory(\App\Models\Insurance::class)->make();
        //test 200
        $response = $this->post("insurance/add", [
            'provider' => $insuranceData->provider,
            'reference_id' => $insuranceData->reference_id,
            'vehicle_uuid' => $vehicleData->uuid,
            'type' => 'cptl',
            'details' => [
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 18700,
                    'amount_without_aon' => 18700,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 200000,
                    'amount_with_aon' => 420,
                    'amount_without_aon' => 420,
                ],
                [
                    'coverage' => str_random(10),
                    'amount_issued' => 850000,
                    'amount_with_aon' => 4250,
                    'amount_without_aon' => 0,
                ],
            ]
        ], $this->AdminHeader($user));

        $insuranceQuote = $this->decode($response)->model;

        //test 200
        $response = $this->post("insurance/finalize", [
            'uuid' => $insuranceQuote->uuid,
            'registration_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'initial_payment' => 8000,
            'has_availed' => 'with_aon',
            'provision_no' => str_random(10)
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);

        //test 403
        $response = $this->post("insurance/amortisation/unpaid/list");
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);

        //test 422
        $response = $this->post("insurance/amortisation/unpaid/list", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "uuid",
        ]);

        //test 200
        $response = $this->post("insurance/amortisation/unpaid/list", [
            'uuid' => $insuranceQuote->uuid,
        ], $this->AdminHeader($user));
        $unpaidData = $this->decode($response)->list;

        $firstUnpaidAmortisation = $unpaidData[0];
        $response = $this->post("payment/add", [
            'amortisation_uuid' => $firstUnpaidAmortisation->uuid,
            'reference' => \App\Helpers\Helper::generateAutoTransactionNo(), //OR number
            'amount' => 16000,
            'payment_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    public function testCheckUnpaidAmortisationsOnPayment(){
        $user = factory(\App\Models\Admin::class)->make();
        // TEST 403
        $response = $this->post("payment/check/amortisation", []);
        $response->assertResponseStatus(403);

        //test 422
        $response = $this->post("payment/check/amortisation", [], $this->AdminHeader($user));
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'uuid',
        ]);

        $insuranceQuote = $this->getFinalizedInsurance();
        //test 200
        $response = $this->post("insurance/amortisation/unpaid/list", [
            'uuid' => $insuranceQuote->uuid,
        ], $this->AdminHeader($user));
        $unpaidData = $this->decode($response)->list;

        $firstUnpaidAmortisation = $unpaidData[0];
        //test 200
        $response = $this->post("payment/check/amortisation", [
            "uuid" => $firstUnpaidAmortisation->uuid
        ], $this->AdminHeader($user));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'proceed',
        ]);

    }

    //analytics
    public function testGetCountAnalytics(){
        // TEST 403
        $response = $this->post("analytics/counts", []);
        $response->assertResponseStatus(403);

        // TEST 200
        $user = factory(\App\Models\Admin::class)->make();
        $response = $this->post("analytics/counts", [], $this->AdminHeader($user)); 
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'total_payments',
            'expiring_insurances',
            'total_customers',
            'avg_quotes',
            'avg_final'
        ]);
    }
}
