FROM php:7.2-apache

RUN apt-get update && apt-get install -y zip git libfreetype6-dev libjpeg62-turbo-dev libpng-dev vim libxml2-dev

COPY . /var/www/html
COPY .docker/php/vhost.conf /etc/apache2/sites-available/000-default.conf

RUN chown -R www-data:www-data /var/www/html
RUN a2enmod rewrite headers

RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install soap

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN chown -R www-data:www-data /usr/local/bin/php

COPY .docker/php/php.ini /usr/local/etc/php/php.ini
RUN chmod u+x /usr/local/etc/php/php.ini

COPY .docker/php/start.sh /usr/local/bin/
RUN chmod u+x /usr/local/bin/start.sh

WORKDIR /var/www/html
RUN touch storage/logs/lumen.log && \
    chmod 777 storage/logs/lumen.log && \
    composer install --optimize-autoloader
CMD ["/usr/local/bin/start.sh"]
