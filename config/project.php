<?php

return [
    "public-key" => [
        "admin" => file_get_contents(storage_path("jwt/keys/admin-auth.key.pub")),
        "agent" => file_get_contents(storage_path("jwt/keys/agent-auth.key.pub")),
        "customer" => file_get_contents(storage_path("jwt/keys/customer-auth.key.pub")),
    ],
    "private-key" => [
        "admin" => file_get_contents(storage_path("jwt/keys/admin-auth.key")),
        "agent" => file_get_contents(storage_path("jwt/keys/agent-auth.key")),
        "customer" => file_get_contents(storage_path("jwt/keys/customer-auth.key")),
    ],
    "message-403" => "Access Forbidden",
    "message-419" => "Token expired",
];
