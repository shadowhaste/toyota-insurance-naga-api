<?php


namespace App\Models;


use App\Services\VehicleService;
use Illuminate\Database\Eloquent\Builder;

class Insurance extends Model
{
    public const CPTL = 'cptl';
    public const COMPREHENSIVE = 'comprehensive';
    public const WITH_AON = 'with_aon';
    public const WITHOUT_AON = 'without_aon';
    public const FINAL = 1;
    public const QUOTE = 0;
    public const IS_EXPIRED = 1;
    public const PAYMENT_DURATION = 3;

    const SORT = "created_at";
    const FIELDS = [
        'uuid',
        'provider',
        'reference_id',
        'vehicle_uuid',
        'registration_date',
        'expiration_date',
        'reminder_date_whole',
        'reminder_date_half',
        'total_amount_issued',
        'total_amount_with_aon',
        'total_amount_without_aon',
        'status',
        'is_expired',
        'type',
        'payable_duration',
        'provision_no',
        'initial_payment',
        'has_availed'
    ];

    protected $fillable = [
        'uuid',
        'provider',
        'reference_id',
        'vehicle_id',
        'vehicle_uuid',
        'registration_date',
        'expiration_date',
        'reminder_date_whole',
        'reminder_date_half',
        'total_amount_issued',
        'total_amount_with_aon',
        'total_amount_without_aon',
        'status',
        'is_expired',
        'type',
        'payable_duration',
        'provision_no',
        'initial_payment',
        'has_availed'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        "customer",
    ];


    public function details(){
        return $this->hasMany(InsuranceDetail::class);
    }

    public function amortisations(){
        return $this->hasMany(Amortisation::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class)->withDefault();
    }

    public function getCustomerAttribute() {
        $vehicle = app()->make(VehicleService::class)->find('uuid', $this->vehicle_uuid)->model;
        return $vehicle->customer;
    }

}