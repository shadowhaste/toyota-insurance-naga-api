<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class Amortisation extends Model
{
    public const PAID = 1;
    public const UNPAID = 0;
    const SORT = "created_at";
    const FIELDS = [
        'id',
        'uuid',
        'insurance_id',
        'insurance_uuid',
        'due_date',
        'amount',
        'is_paid',
        'payment_reference'
    ];

    protected $fillable = [
        'uuid',
        'insurance_id',
        'insurance_uuid',
        'due_date',
        'amount',
        'is_paid',
        'payment_reference'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function insurance()
    {
        return $this->belongsTo(Insurance::class)->withDefault();
    }

    public function payments(){
        return $this->hasMany(Payment::class);
    }

}