<?php

namespace App\Models;

class Customer extends Model {
    const SORT = "created_at";
    const FIELDS = [
        'uuid',
        'email',
        'lastname',
        'firstname',
        'middlename',
        'phone_number',
        'mobile_number',
    ];

    protected $fillable = [
        'uuid',
        'email',
        'tin',
        'lastname',
        'firstname',
        'middlename',
        'suffix',
        'address',
        'phone_number',
        'mobile_number',
        'facebook_id',
        'birth_date',
        'gender',
        'civil_status',
        'height',
        'weight',

        'occupation',
        'current_employer',
        'current_employer_address',
        'industry',
        'office_phone_number',
        'email_or_website',

        'spouse_lastname',
        'spouse_firstname',
        'spouse_middlename',
        'spouse_suffix',
        'spouse_mobile_number',
        'spouse_occupation',
        'spouse_current_employer',
        'spouse_current_employer_address',
        'spouse_industry',
        'spouse_office_phone_number',
        'spouse_email_or_website',
        'children'
    ];
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];


}
