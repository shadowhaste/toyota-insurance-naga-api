<?php

namespace App\Models;

class Admin extends User {

    protected $fillable = [
        'uuid',
        'username',
        'password',
    ];
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'password'
    ];

    /**
     * User to Token relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function token() {
        return $this->hasOne(AdminToken::class, 'user_id');
    }

}
