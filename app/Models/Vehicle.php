<?php


namespace App\Models;


class Vehicle extends Model
{
    const SORT = "created_at";
    const FIELDS = [
        'uuid',
        'customer_uuid',
        'model_no',
        'make',
        'description',
        'color',
        'cs_no',
        'vin',
        'vsi',
        'engine',
        'displacement',
        'year_model',
        'gross_weight',
        'key_number',
        'release_date'
    ];

    protected $fillable = [
        'uuid',
        'customer_id',
        'customer_uuid',
        'model_no',
        'make',
        'description',
        'color',
        'cs_no',
        'vin',
        'vsi',
        'engine',
        'displacement',
        'year_model',
        'gross_weight',
        'key_number',
        'release_date'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function insurances(){
        return $this->hasMany(Insurance::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withDefault();
    }
}