<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class Payment extends Model
{
    public const PAID = 1;
    public const UNPAID = 0;
    const SORT = "created_at";
    const FIELDS = [
        'uuid',
        'amortisation_id',
        'amortisation_uuid',
        'reference',
        'amount',
        'payment_date',
    ];

    protected $fillable = [
        'uuid',
        'amortisation_id',
        'amortisation_uuid',
        'reference',
        'amount',
        'payment_date',
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function amortisation()
    {
        return $this->belongsTo(Amortisation::class)->withDefault();
    }
}