<?php


namespace App\Models;


class InsuranceDetail extends Model
{
    const SORT = "created_at";
    const FIELDS = [

    ];

    protected $fillable = [
        'uuid',
        'insurance_id',
        'insurance_uuid',
        'coverage',
        'amount_issued',
        'amount_with_aon',
        'amount_without_aon'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

}