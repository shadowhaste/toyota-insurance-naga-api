<?php

namespace App\Repositories;


use App\Models\Customer;
use Carbon\Carbon;

class CustomerRepository extends Repository {

    public function __construct(Customer $model) {
        parent::__construct($model);
    }

    public function findFiltered($data){
        $data = (object) $data;
        $list = $this->model;

        if(isset($data->keyword)){
            $list = $list->where(function ($query) use ($data) {
                $query->where("firstname", "LIKE", "%{$data->keyword}%")
                    ->orWhere("lastname", "LIKE", "%{$data->keyword}%");
            });
        }

        if(isset($data->dateFrom) && isset($data->dateTo)){
            try{
                $dateFrom = Carbon::now()->parse($data->dateFrom)->format('Y-m-d 00:00:00');
                $dateTo = Carbon::now()->parse($data->dateTo)->format('Y-m-d 23:59:59');
                $list = $list->whereBetween('created_at', [$dateFrom, $dateTo]);
            }catch(\Exception $e){

            }

        }

        return $list;
    }

    public function totalCustomers(){
        return $this->model->count();
    }
}
