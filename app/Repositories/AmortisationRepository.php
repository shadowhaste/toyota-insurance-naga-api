<?php

namespace App\Repositories;


use App\Models\Amortisation;
use Carbon\Carbon;

class AmortisationRepository extends Repository {

    public function __construct(Amortisation $model) {
        parent::__construct($model);
    }

    public function unpaidAmortisations($insuranceUuid){
        return $this->model
                    ->where('insurance_uuid', $insuranceUuid)
                    ->where('is_paid', 0)
                    ->get();
    }

    public function find($field, $id) {
        return $this->model->where($field, $id)->with('payments')->first();
    }
}
