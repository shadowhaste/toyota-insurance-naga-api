<?php

namespace App\Repositories;


use App\Models\Vehicle;
use Carbon\Carbon;

class VehicleRepository extends Repository {

    public function __construct(Vehicle $model) {
        parent::__construct($model);
    }

    public function findFiltered($data){
        $data = (object) $data;
        $list = $this->model->where('customer_uuid', $data->customer_uuid);

        if(isset($data->keyword)){
            $list = $list->where(function ($query) use ($data) {
                $query->where("model_no", "LIKE", "%{$data->keyword}%")
                    ->orWhere("make", "LIKE", "%{$data->keyword}%")
                    ->orWhere("description", "LIKE", "%{$data->keyword}%")
                    ->orWhere("color", "LIKE", "%{$data->keyword}%")
                    ->orWhere("cs_no", "LIKE", "%{$data->keyword}%")
                    ->orWhere("vin", "LIKE", "%{$data->keyword}%")
                    ->orWhere("vsi", "LIKE", "%{$data->keyword}%");
            });
        }

        if(isset($data->dateFrom) && isset($data->dateTo)){
            try{
                $dateFrom = Carbon::now()->parse($data->dateFrom)->format('Y-m-d 00:00:00');
                $dateTo = Carbon::now()->parse($data->dateTo)->format('Y-m-d 23:59:59');
                $list = $list->whereBetween('created_at', [$dateFrom, $dateTo]);
            }catch(\Exception $e){

            }
        }

        return $list;
    }
}
