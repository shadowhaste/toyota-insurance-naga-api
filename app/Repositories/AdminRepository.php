<?php

namespace App\Repositories;

use App\Models\Admin as Model;

class AdminRepository extends Repository {

    public function __construct(Model $model) {
        $this->model = $model;
    }

}
