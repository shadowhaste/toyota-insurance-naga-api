<?php

namespace App\Repositories;

use App\Models\AdminToken as Token;

class AdminTokenRepository extends TokenRepository {

    public function __construct(Token $token) {
        $this->token = $token;
    }

}
