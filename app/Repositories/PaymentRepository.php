<?php

namespace App\Repositories;


use App\Models\Payment;
use Carbon\Carbon;

class PaymentRepository extends Repository {

    public function __construct(Payment $model) {
        parent::__construct($model);
    }

    public function totalPayments(){
        return $this->model->sum('amount');
    }
}
