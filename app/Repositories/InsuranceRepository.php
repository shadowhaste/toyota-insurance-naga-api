<?php

namespace App\Repositories;


use App\Models\Insurance;
use Carbon\Carbon;

class InsuranceRepository extends Repository {

    public function __construct(Insurance $model) {
        parent::__construct($model);
    }

    public function find($field, $id) {
        return $this->model->where($field, $id)->with(['details', 'amortisations', 'vehicle'])->first();
    }

    public function activeInsurance($vehicleUuid, $type){
        return $this->model
                    ->where('vehicle_uuid', $vehicleUuid)
                    ->where('type', $type)
                    ->where('is_expired', 0)
                    ->where('status', Insurance::FINAL)
                    ->first();
    }

    public function findFiltered($data){
        $data = (object) $data;

        $list = $this->model->where('vehicle_uuid', $data->vehicle_uuid);

        if(isset($data->keyword) && $data->keyword !== ''){
            $list = $list->where(function ($query) use ($data) {
                $query->where("reference_id", "LIKE", "%{$data->keyword}%");
                $query->orWhere("provider", "LIKE", "%{$data->keyword}%");

            });
        }

        if(isset($data->dateFrom) && isset($data->dateTo)){
            try{
                $dateFrom = Carbon::now()->parse($data->dateFrom)->format('Y-m-d 00:00:00');
                $dateTo = Carbon::now()->parse($data->dateTo)->format('Y-m-d 23:59:59');
                $list = $list->whereBetween('created_at', [$dateFrom, $dateTo]);
            }catch(\Exception $e){

            }
        }

        return $list;
    }

    public function updateExpiredInsurances($date){
        return $this->model
                    ->where('is_expired', 0)
                    ->where('status', Insurance::FINAL)
                    ->whereDate('expiration_date', $date)
                    ->update([
                        "is_expired" => 1
                    ]);
    }

    public function getInsuranceRemindingDate($date, $iswhole = false){
        $remindingDate = ($iswhole) ? "reminder_date_whole" : "reminder_date_half";
        return $this->model
                    ->whereDate($remindingDate, $date)
                    ->where('status', Insurance::FINAL)
                    ->get();
    }

    public function activeInsuranceCount(){
        return $this->model
            ->where('is_expired', 0)
            ->where('status', Insurance::FINAL)
            ->count();
    }

    public function quoteInsuranceCount(){
        return $this->model
            ->where('is_expired', 0)
            ->where('status', Insurance::QUOTE)
            ->count();
    }
}
