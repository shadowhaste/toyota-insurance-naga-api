<?php

namespace App\Repositories;


use App\Models\InsuranceDetail;
use Carbon\Carbon;

class InsuranceDetailRepository extends Repository {

    public function __construct(InsuranceDetail $model) {
        parent::__construct($model);
    }

}
