<?php


namespace App\Console\Commands;

use App\Repositories\InsuranceRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ExpireInsuranceCommand extends Command
{
    protected $signature = "expire:insurance";
    protected $insuranceRepository;
    public function __construct(InsuranceRepository $insuranceRepository)
    {
        $this->insuranceRepository = $insuranceRepository;
        parent::__construct();
    }

    public function handle() {
        Log::info('Running Expire Insurance command - START');
        $curdate = Carbon::now()->format('Y-m-d');
        $update = $this->insuranceRepository->updateExpiredInsurances($curdate);
        Log::Debug('expired insurance: ', [$update]);

        //remind insurance customer
        //whole
        $this->remind($curdate, true);

        //half
        $this->remind($curdate);

        Log::info('Running Expire Insurance command - END');
    }

    private function remind($date, $isWholeDate = false){
        $insurances = $this->insuranceRepository->getInsuranceRemindingDate($date, $isWholeDate);
        if($insurances != null){
            foreach ($insurances as $insurance){
                $vehicle = $insurance->vehicle;
                $customer = $vehicle->customer;

                $message = ($isWholeDate) ? __("messages.sms.reminder.whole") : __("messages.sms.reminder.half");
                $message = str_replace('CUSTOMER_NAME', $customer->firstname . ' ' . $customer->lastname, $message);
                $message = str_replace('VEHICLE_MODEL', $vehicle->model_no, $message);
                $message = str_replace('INSURANCE_REFERENCE', $insurance->reference_id, $message);
                $message = str_replace('EXPIRATION_DATE', $insurance->expiration_date, $message);

                $this->sendSms($customer->mobile_number, $message);
            }
        }
    }

    private function sendSms($mobile, $message){
        Log::debug($mobile);
        Log::debug($message);
    }
}