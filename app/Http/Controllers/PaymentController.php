<?php


namespace App\Http\Controllers;


use App\Services\PaymentService;
use App\ThirdParty\Jwt\TokenAuthFacades;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $service;
    public function __construct(PaymentService $service)
    {
        $this->service = $service;
    }

    public function create(Request $request){
        TokenAuthFacades::getUser($request, "admin");
        $this->validate($request, [
            'amortisation_uuid' => 'required|uuid',
            'reference' => 'required|unique:payments,reference',
            'amount' => 'required|min:1',
            'payment_date' => 'required',
        ]);

        $result = $this->service->create($request->all());

        return response()->json([
            "message" => $result->message,
            "model" => $result->model
        ], $result->status);

    }

    public function check(Request $request){
        TokenAuthFacades::getUser($request, "admin");
        $this->validate($request, [
            'uuid' => 'required|uuid',
        ]);

        $result = $this->service->check($request->all());

        return response()->json([
            "message" => $result->message,
            "proceed" => $result->model
        ], $result->status);
    }
}