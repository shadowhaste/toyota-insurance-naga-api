<?php


namespace App\Http\Controllers;


use App\Services\AmortisationService;
use App\ThirdParty\Jwt\TokenAuthFacades;
use Illuminate\Http\Request;

class AmortisationController extends Controller
{
    protected $service;
    public function __construct(AmortisationService $service)
    {
        $this->service = $service;
    }

    public function getUnpaidList(Request $request){
        $admin = TokenAuthFacades::getUser($request, 'admin');
        $this->validate($request, [
            'uuid' => 'required',
        ]);

        $getList = $this->service->unpaidList($request->uuid);
        return response()->json([
            "list" => $getList->list,
            "message" => $getList->message,
        ], 200);

    }

    public function view(Request $request){
        $admin = TokenAuthFacades::getUser($request, 'admin');
        $this->validate($request, [
            'uuid' => 'required',
        ]);

        $result = $this->service->find("uuid", $request->uuid);

        return response()->json([
            "message" => $result->message,
            "model" => $result->model

        ], $result->status);

    }
}