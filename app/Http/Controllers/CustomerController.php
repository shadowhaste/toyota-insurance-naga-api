<?php


namespace App\Http\Controllers;

use App\Services\CustomerService;
use App\ThirdParty\Jwt\TokenAuthFacades;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class CustomerController extends Controller
{
    protected $service;
    public function __construct(CustomerService $service)
    {
        $this->service = $service;
    }

    public function create(Request $request){
        TokenAuthFacades::getUser($request, "admin");

        $this->validate($request, [
            'email' => 'required|unique:customers',
            'lastname' => 'required',
            'firstname' => 'required',
            'middlename' => 'required',
            'mobile_number' => 'required',
            'occupation' => 'required',
        ]);

        $data = $request->all();
        $result = $this->service->create($data);

        return response()->json([
            "message" => $result->message,
            "model" => $result->model
        ], $result->status);
    }

    public function getList(Request $request, $page = 1) {
        $admin = TokenAuthFacades::getUser($request, 'admin');
        $getList = $this->service->getList($request->all(), $page);

        if ($getList->status == 200) {
            return response()->json([
                "list" => $getList->list,
                "max_page" => $getList->max_page,
                "prev_page" => $getList->prev_page,
                "next_page" => $getList->next_page,
                "message" => $getList->message,
            ]);
        }

        return response()->json([
            "list" => null,
            "max_page" => null,
            "prev_page" => null,
            "next_page" => null,
            "message" => $getList->message,
        ], $getList->status);
    }

    public function view(Request $request){
        TokenAuthFacades::getUser($request, "admin");
        $this->validate($request, [
            'uuid' => 'required|uuid',
        ]);

        $result = $this->service->find("uuid", $request->uuid);

        return response()->json([
            "message" => $result->message,
            "customer" => $result->model,

        ], $result->status);
    }

}