<?php


namespace App\Http\Controllers;


use App\Services\InsuranceService;
use App\Services\VehicleService;
use App\ThirdParty\Jwt\TokenAuthFacades;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

class InsuranceController extends Controller
{
    protected $service;
    protected $vehicleService;
    public function __construct(InsuranceService $service, VehicleService $vehicleService)
    {
        $this->service = $service;
        $this->vehicleService = $vehicleService;
    }

    public function create(Request $request){
        TokenAuthFacades::getUser($request, "admin");

        $this->validate($request, [
            'provider' => 'required',
            'reference_id' => 'required',
            'vehicle_uuid' => 'required',
            'type' => 'required',

            'details' => 'filled|array',
            'details.*.coverage' => 'required',
            'details.*.amount_issued' =>'required',
            'details.*.amount_with_aon' =>'required',
            'details.*.amount_without_aon' => 'required',
        ]);

        //add to insurance table
        $data = $request->all();

        //check if there is an unexpired existing insurance
        if($this->service->hasActiveInsurance($data['vehicle_uuid'], $data['type'])){
            return response()->json([
                "message" => __("messages.insurance.has_active"),
            ], 400);
        }

        $vehicleObj = $this->vehicleService->find('uuid', $request->vehicle_uuid);
        $data['vehicle_id'] = $vehicleObj->model->id;

        $result = $this->service->create($data);

        return response()->json([
            "message" => $result->message,
            "model" => $result->model
        ], $result->status);

    }

    public function finalize(Request $request){
        TokenAuthFacades::getUser($request, "admin");
        $this->validate($request, [
            'uuid' => 'required',
            'registration_date' => 'required',
            'initial_payment' => 'required|min:1',
            'has_availed' => 'required',
        ]);

        $data = $request->all();
        $data['provision_no'] = $request->input('provision_no');
        $result = $this->service->finalize($data);

        return response()->json([
            "message" => $result->message,
            "model" => $result->model
        ], $result->status);
    }

    public function view(Request $request){
        TokenAuthFacades::getUser($request, "admin");
        $this->validate($request, [
            'uuid' => 'required|uuid',
        ]);

        $result = $this->service->find("uuid", $request->uuid);
        
        return response()->json([
            "message" => $result->message,
            "insurance" => $result->model

        ], $result->status);
    }

    public function getList(Request $request, $page = 1){
        $admin = TokenAuthFacades::getUser($request, 'admin');
        $this->validate($request, [
            'vehicle_uuid' => 'required',
        ]);

        $getList = $this->service->getList($request->all(), $page);

        if ($getList->status == 200) {
            return response()->json([
                "list" => $getList->list,
                "max_page" => $getList->max_page,
                "prev_page" => $getList->prev_page,
                "next_page" => $getList->next_page,
                "message" => $getList->message,
            ]);
        }

        return response()->json([
            "list" => null,
            "max_page" => null,
            "prev_page" => null,
            "next_page" => null,
            "message" => $getList->message,
        ], $getList->status);
    }

}