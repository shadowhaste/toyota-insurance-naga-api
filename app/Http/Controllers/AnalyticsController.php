<?php


namespace App\Http\Controllers;


use App\Services\CustomerService;
use App\Services\InsuranceService;
use App\Services\PaymentService;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    protected $paymentService;
    protected $customerService;
    protected $insuranceService;

    public function __construct(
        PaymentService $paymentService,
        CustomerService $customerService,
        InsuranceService $insuranceService
    )
    {
        $this->paymentService = $paymentService;
        $this->customerService = $customerService;
        $this->insuranceService = $insuranceService;
    }

    public function counts(Request $request){

        $averageTotalCount = $this->insuranceService->getAverageInsurances();
        return response()->json([
            'total_payments' => $this->paymentService->totalPayments(),
            'active_insurance' => $this->insuranceService->activeInsuranceCount(),
            'total_customers' => $this->customerService->totalCustomers(),
            'avg_quotes' => $averageTotalCount->final,
            'avg_final' => $averageTotalCount->quote
        ], 200);
    }
}