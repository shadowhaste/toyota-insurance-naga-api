<?php


namespace App\Http\Controllers;


use App\Services\CustomerService;
use App\Services\VehicleService;
use App\ThirdParty\Jwt\TokenAuthFacades;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class VehicleController extends Controller
{
    protected $service;
    protected $customerService;
    public function __construct(VehicleService $service, CustomerService $customerService)
    {
        $this->service = $service;
        $this->customerService = $customerService;
    }

    public function create(Request $request){
        TokenAuthFacades::getUser($request, "admin");

        $this->validate($request, [
            'customer_uuid' => 'required',
            'model_no' => 'required|unique:vehicles',
            'make' => 'required',
            'color' => 'required',
            'cs_no' => 'required',
            'vin' => 'required',
            'vsi' => 'required',
            'engine' => 'required',
            'release_date' => 'required'
        ]);


        $data = $request->all();
        $customerObj = $this->customerService->find('uuid', $request->customer_uuid);
        $data['customer_id'] = $customerObj->model->id;

        $result = $this->service->create($data);

        return response()->json([
            "message" => $result->message,
            "model" => $result->model
        ], $result->status);
    }

    public function getList(Request $request, $page = 1) {
        $admin = TokenAuthFacades::getUser($request, 'admin');

        $this->validate($request, [
            'customer_uuid' => 'required',
        ]);

        $getList = $this->service->getList($request->all(), $page);

        if ($getList->status == 200) {
            return response()->json([
                "list" => $getList->list,
                "max_page" => $getList->max_page,
                "prev_page" => $getList->prev_page,
                "next_page" => $getList->next_page,
                "message" => $getList->message,
            ]);
        }

        return response()->json([
            "list" => null,
            "max_page" => null,
            "prev_page" => null,
            "next_page" => null,
            "message" => $getList->message,
        ], $getList->status);
    }

    public function view(Request $request){
        TokenAuthFacades::getUser($request, "admin");
        $this->validate($request, [
            'uuid' => 'required|uuid',
        ]);

        $result = $this->service->find("uuid", $request->uuid);

        return response()->json([
            "message" => $result->message,
            "vehicle" => $result->model,

        ], $result->status);
    }
}