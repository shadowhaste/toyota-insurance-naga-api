<?php

namespace App\Services;

use App\Models\Amortisation;
use App\Models\Insurance;
use App\Repositories\AmortisationRepository;
use App\Repositories\InsuranceDetailRepository;
use App\Repositories\InsuranceRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

class InsuranceService extends Service {

    protected $module = 'insurance';

    const PERPAGE = 10;

    protected $insuranceDetailRepository;
    protected $amortisationRepository;
    protected $paymentService;

    public function __construct(
        InsuranceRepository $repository,
        InsuranceDetailRepository $insuranceDetailRepository,
        AmortisationRepository $amortisationRepository,
        PaymentService $paymentService
    ) {
        parent::__construct($repository);
        $this->insuranceDetailRepository = $insuranceDetailRepository;
        $this->amortisationRepository = $amortisationRepository;
        $this->paymentService = $paymentService;
    }

    public function find($field, $id) {
        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.view.200"),
            "model" => $this->repository->find($field, $id)
        ];
    }

    public function create($data) {


        $insuranceDetails = $data['details'];

        $data['total_amount_issued'] = array_sum(array_column($insuranceDetails, 'amount_issued'));
        $data['total_amount_with_aon'] = array_sum(array_column($insuranceDetails, 'amount_with_aon'));
        $data['total_amount_without_aon'] = array_sum(array_column($insuranceDetails, 'amount_without_aon'));

        $created = $insurance = $this->repository->create($data);

        $generatedDetails = [];
        foreach ($insuranceDetails as $insuranceDetail){
            $insuranceDetail['uuid'] = Uuid::uuid4()->toString();
            $insuranceDetail['insurance_id'] = $insurance->id;
            $insuranceDetail['insurance_uuid'] = $insurance->uuid;

            $generatedDetails[] = $insuranceDetail;
        }

        //bulk insert details
        $this->insuranceDetailRepository->insert($generatedDetails);

        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.create.200"),
            "model" => $created
        ];
    }

    public function finalize($data){

        //get insurance
        $insurance = $this->repository->find('uuid', $data['uuid']);
        if($insurance != null){

            //check if insurance is already finalized
            if($insurance->status == Insurance::FINAL){
                return (object) [
                    "status" => 400,
                    "message" => __("messages.{$this->module}.finalize.already"),
                    "model" => null
                ];
            }


            $insurance->registration_date = $data['registration_date'];
            $expirationCount = ($insurance->type == Insurance::CPTL) ? 3: 1;
            $expirationDate = Carbon::parse($insurance->registration_date)->addYear($expirationCount);

            $insurance->expiration_date = $expirationDate->format('Y-m-d');
            $insurance->reminder_date_whole = $expirationDate->subDays(30)->format('Y-m-d');
            $insurance->reminder_date_half = $expirationDate->subDays(15)->format('Y-m-d');

            $insurance->initial_payment = $data['initial_payment'];
            $insurance->has_availed = $data['has_availed'];
            $insurance->payable_duration = Insurance::PAYMENT_DURATION;
            $insurance->provision_no = $data['provision_no'];
            $insurance->status = Insurance::FINAL;


            //generate amortisation based on remaining balance and duration
            //get total based on what availed
            $totalAmountPayable = ($insurance->has_availed == Insurance::WITH_AON) ? $insurance->total_amount_with_aon : $insurance->total_amount_without_aon;

            //get monthly payable
            $monthlyPayable = $totalAmountPayable / $insurance->payable_duration;

            //check if initial payment is less than monthly payable
            if($insurance->initial_payment < $monthlyPayable){
                return (object) [
                    "status" => 400,
                    "message" => __("messages.{$this->module}.finalize.lack_initial_payment"),
                    "model" => null
                ];
            }

            $amortisations = [];

            //substract initial payment to total amount payable
            $totalAmountPayable = $totalAmountPayable - $insurance->initial_payment;

            //add first amortisation tagged as paid
            $amortisations[] = [
                'insurance_id' => $insurance->id,
                'insurance_uuid' => $insurance->uuid,
                'due_date' => Carbon::parse($insurance->registration_date)->addMonth(1)->format('Y-m-d'),
                'amount' => $insurance->initial_payment,
                'is_paid' => Amortisation::PAID,
            ];


            //get remaining duration
            $remainingDurations = Insurance::PAYMENT_DURATION - 1;

            //update monthly payable
            $monthlyPayable = $totalAmountPayable / $remainingDurations;

            //populate to remaining months
            for($i=1; $i<=$remainingDurations; $i++){
                $amortisations[] = [
                    'insurance_id' => $insurance->id,
                    'insurance_uuid' => $insurance->uuid,
                    'due_date' => Carbon::parse($insurance->registration_date)->addMonth($i + 1)->format('Y-m-d'),
                    'amount' => $monthlyPayable,
                    'is_paid' => Amortisation::UNPAID,
                ];
            };

            //save insurance
            if($insurance->save()){
                //generate amortisation
                $this->generateAmortisations($amortisations);
            }

        }

        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.finalize.200"),
            "model" => $insurance
        ];
    }

    public function hasActiveInsurance($vehicleUuid, $type){
        $insurance = $this->repository->activeInsurance($vehicleUuid, $type);
        return ($insurance != null) ? true: false;
    }

    public function getList($data, $page) {
        $list = $this->repository->findFiltered($data);
        return $this->getResponseList($list, $page);
    }

    public function generateAmortisations($data){
        if(!empty($data)){
            foreach ($data as $datum){
                $created = $this->amortisationRepository->create($datum);
                if($created->is_paid == Amortisation::PAID){
                    //add payment
                    $this->paymentService->pay($created, $created->amount);
                }
            }
        }
    }

    public function activeInsuranceCount(){
        return $this->repository->activeInsuranceCount();
    }

    public function quoteInsuranceCount(){
        return $this->repository->quoteInsuranceCount();
    }

    public function getAverageInsurances(){
        $final = $this->activeInsuranceCount();
        $quote = $this->quoteInsuranceCount();

        $total = $final + $quote;

        if($total > 0){
            return (object)[
                "final" => ($final/$total) * 100,
                "quote" => ($quote/$total) * 100
            ];
        }

        return (object)[
            "final" => 0,
            "quote" => 0
        ];
    }
}
