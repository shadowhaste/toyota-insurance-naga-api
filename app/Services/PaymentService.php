<?php

namespace App\Services;

use App\Helpers\Helper;
use App\Models\Amortisation;
use App\Repositories\PaymentRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class PaymentService extends Service {

    const PERPAGE = 10;
    protected $module = 'payment';
    protected $amortisationService;

    public function __construct(PaymentRepository $repository, AmortisationService $amortisationService) {
        parent::__construct($repository);
        $this->amortisationService = $amortisationService;
    }

    public function pay(Amortisation $amortisation, $amount, $reference = null, $paymentDate = null){
        $paymentReference = ($reference == null) ? Helper::generateAutoTransactionNo() : $reference;
        $paymentDate = ($paymentDate == null) ? Carbon::now()->format('Y-m-d') : $paymentDate;
        $payment = $this->repository->create([
            'amortisation_id' => $amortisation->id,
            'amortisation_uuid' => $amortisation->uuid,
            'reference' => $paymentReference,
            'amount' => $amount,
            'payment_date' => $paymentDate,
        ]);

        //update amortisation payment reference
        $amortisation->payment_reference = $paymentReference;
        $amortisation->is_paid = ($amount < $amortisation->amount) ? Amortisation::UNPAID : Amortisation::PAID;

        //check if insurance is free
        if($amortisation->insurance->provision_no != null){
            $amortisation->is_paid = Amortisation::PAID;
        }

        $amortisation->save();

        return $payment;
    }

    public function create($data)
    {
        $data = (object) $data;
        $payment = $data->amount;

        $amortisation = $this->amortisationService->find('uuid', $data->amortisation_uuid);

        //make sure amortisation is the first unpaid
        $unpaidList = $this->amortisationService->unpaidList($amortisation->model->insurance_uuid);
        $key = array_search($amortisation->model->uuid, array_column($unpaidList->list->toArray(), 'uuid'));
        if($key > 0){
            return (object) [
                "status" => 400,
                "message" => __("messages.{$this->module}.cannot"),
                "model" => null
            ];
        }

        if($payment < $amortisation->model->amount){
            return (object) [
                "status" => 400,
                "message" => __("messages.{$this->module}.amount.less"),
                "model" => null
            ];
        }

        //pay the given amortisation
        $firstPay = $this->pay($amortisation->model, $amortisation->model->amount, $data->reference, $data->payment_date);
        $payment = $payment - $amortisation->model->amount;

        if($payment == 0){
            return (object) [
                "status" => 200,
                "message" => __("messages.{$this->module}.created.200"),
                "model" => $firstPay
            ];
        }

        //get unpaid amortisations
        $unpaidList = $this->amortisationService->unpaidList($amortisation->model->insurance_uuid);

        //process remaining amortisations
        foreach ($unpaidList->list as $item){
            if($payment == 0)
                continue;

            if($item->uuid == $amortisation->model->uuid)
                continue;

            $amortisation = $item;
            if($payment < $amortisation->amount){
                $this->pay($amortisation, $payment, $data->reference, $data->payment_date);
                $amortisation->amount = $amortisation->amount - $payment;
                $amortisation->save();

                $payment = 0;
            } else {
                $this->pay($amortisation, $amortisation->amount, $data->reference, $data->payment_date);
                $payment = $payment - $amortisation->amount;
            }
        }

        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.created.200"),
            "model" => null
        ];

    }

    public function check($data)
    {
        $data = (object) $data;

        $amortisation = $this->amortisationService->find('uuid', $data->uuid);
        //make sure amortisation is the first unpaid
        $unpaidList = $this->amortisationService->unpaidList($amortisation->model->insurance_uuid);

        $key = array_search($amortisation->model->uuid, array_column($unpaidList->list->toArray(), 'uuid'));
        if($key > 0){
            return (object) [
                "status" => 400,
                "message" => __("messages.{$this->module}.cannot"),
                "model" => 0
            ];
        }

        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.checked.200"),
            "model" => 1
        ];
    }

    public function totalPayments(){
        return $this->repository->totalPayments();
    }
}
