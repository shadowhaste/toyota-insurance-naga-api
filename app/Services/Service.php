<?php

namespace App\Services;

use App\Repositories\Repository;
use App\Models\Model;

abstract class Service {

    protected $repository;
    protected $module = 'default';
    const PERPAGE = 10;

    public function __construct(Repository $repository) {
        $this->repository = $repository;
    }

    public function create($data) {
        return (object) [
                    "status" => 200,
                    "message" => __("messages.{$this->module}.create.200"),
                    "model" => $this->repository->create($data)
        ];
    }

    public function update(Model $model, $data) {
        return (object) [
                    "status" => 200,
                    "message" => __("messages.{$this->module}.update.200"),
                    "model" => $this->repository->update($model, $data),
        ];
    }

    public function delete(Model $model) {
        $delete = $this->repository->delete($model);
        if ($delete) {
            return (object) [
                        "status" => 200,
                        "message" => __("messages.{$this->module}.delete.200")
            ];
        }

        return (object) [
                    "status" => 400,
                    "message" => __("messages.{$this->module}.delete.400")
        ];
    }

    public function view(Model $model) {
        return $this->repository->view($model);
    }

    public function find($field, $id) {
        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.view.200"),
            "model" => $this->repository->find($field, $id)
        ];
    }

    public function findNext($givenId, $field, $value) {
        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.view.200"),
            "model" => $this->repository->findNext($givenId, $field, $value)
        ];
    }

    public function findBy($field, $value, $columns) {

        $collection = $this->repository->findBy($field, $value, $columns);
        return (object) [
                    "status" => 200,
                    "message" => __("messages.{$this->module}.view.200"),
                    "list" => $collection
        ];
    }

    public function getResponseList($list, $page){
        $count = $list->count();

        // IF EMPTY PAGE //
        if ($page == 1 && $count == 0) {
            return (object) [
                "message" => __("messages.{$this->module}.index.empty"),
                "status" => 200,
                "list" => [],
                "max_page" => 1,
                "prev_page" => 0,
                "next_page" => 0,
            ];
        }

        $max_page = ceil($count / static::PERPAGE);
        if ($page > $max_page) {
            return (object) [
                "status" => 404,
                "message" => __("messages.{$this->module}.index.404")
            ];
        }

        return (object) [
            "message" => null,
            "status" => 200,
            "list" => $list->byPage($page, self::PERPAGE),
            "max_page" => $max_page,
            "prev_page" => $page == 1 ? 0 : $page - 1,
            "next_page" => $page == $max_page ? 0 : $page + 1
        ];
    }
}
