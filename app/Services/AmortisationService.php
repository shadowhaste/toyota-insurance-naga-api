<?php

namespace App\Services;

use App\Models\Insurance;
use App\Repositories\AmortisationRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

class AmortisationService extends Service {

    protected $module = 'amortisation';
    const PERPAGE = 10;

    public function __construct(AmortisationRepository $repository) {
        parent::__construct($repository);
    }

    public function unpaidList($uuid){
        return (object) [
            "status" => 200,
            "message" => __("messages.{$this->module}.list"),
            "list" => $this->repository->unpaidAmortisations($uuid)
        ];
    }
}
