<?php

namespace App\Services;

use Illuminate\Support\Facades\Hash;
use App\Repositories\AdminRepository as Repository;

class AdminService extends UserService {

    protected $module = 'admin';

    public function __construct(Repository $repository) {
        $this->repository = $repository;
        $this->userType = 'admin';
    }

    public function login($username, $password) {
        $user = $this->repository->find('username', $username);

        if (!$user) {
            return (object) [
                        "status" => 404,
                        "message" => __("messages.user.login.404"),
                        "token" => null,
                        "user" => null,
            ];
        }

        if (Hash::check($password, $user->password)) {
            return (object) [
                        "status" => 200,
                        "message" => null,
                        "token" => $this->generateJWTToken($user),
                        "user" => $user,
                        "refresh_token" => $user->generateRefreshToken(),
            ];
        }

        return (object) [
                    "status" => 401,
                    "message" => __("messages.user.login.401"),
                    "token" => null,
                    "user" => null,
                    "refresh_token" => null,
        ];
    }

}
