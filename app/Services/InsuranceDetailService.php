<?php

namespace App\Services;

use App\Repositories\InsuranceDetailRepository;

class InsuranceDetailService extends Service {

    protected $module = 'insurance_detail';
    const PERPAGE = 10;

    public function __construct(InsuranceDetailRepository $repository) {
        parent::__construct($repository);
    }

}
