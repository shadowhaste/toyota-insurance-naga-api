<?php

namespace App\Services;

use App\Repositories\VehicleRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class VehicleService extends Service {

    protected $module = 'vehicle';
    const PERPAGE = 10;

    public function __construct(VehicleRepository $repository) {
        $this->repository = $repository;
    }

    public function getList($data, $page) {
        $list = $this->repository->findFiltered($data);
        return $this->getResponseList($list, $page);
    }

    public function isVehicleBrandNew($uuid){
        $model = $this->repository->find("uuid", $uuid);
        if($model != null){

            $releaseDate = Carbon::parse($model->release_date);
            $duration = Carbon::now()->addDays(3);
            if($releaseDate->lessThan($duration)){
                return true;
            }
        }

        return false;
    }
}
