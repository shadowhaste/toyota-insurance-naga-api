<?php

namespace App\Services;

use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;

class CustomerService extends Service {

    protected $module = 'customer';
    const PERPAGE = 10;

    public function __construct(CustomerRepository $repository) {
        $this->repository = $repository;
    }

    public function getList($data, $page) {
        $list = $this->repository->findFiltered($data);
        return $this->getResponseList($list, $page);
    }

    public function totalCustomers(){
        return $this->repository->totalCustomers();
    }
}
