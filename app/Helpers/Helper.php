<?php

/**
 * Class Helper | app/Helpers/Helper.php
 *
 * @package     Points
 * @subpackage  Points
 * @author      Aries Jay C. Traquena <atraquena@gyondu.com>
 * @version     v.1.0 (08/16/2018)
 * @copyright   Copyright (c) 2018, Traqy
 */

namespace App\Helpers;

use Carbon\Carbon;

/**
 * Class Helper
 *
 * Achievement Unlock Helper
 */
class Helper {
    public static function generateAutoTransactionNo() {
        $date = Carbon::now()->format("Ymd");
        $digits = $date . mt_rand();
        if (strlen($digits) < 50) {
            return intval($digits);
        }
        self::generateAutoTransactionNo();
    }


}
