<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Admin
$router->post("login", "AdminController@login");
$router->post("register", "AdminController@register");
$router->group(['middleware' => "auth:admin", "prefix" => "customer"], function() use ($router) {
    //create
    $router->post("add", "CustomerController@create");
    $router->post("view", ["as" => "customer.view", "uses" => "CustomerController@view"]);
    $router->post("list[/{page:\d+}]", ["as" => "customer.list", "uses" => "CustomerController@getList"]);
});

$router->group(['middleware' => "auth:admin", "prefix" => "vehicle"], function() use ($router) {
    //create
    $router->post("add", "VehicleController@create");
    $router->post("view", ["as" => "vehicle.view", "uses" => "VehicleController@view"]);
    $router->post("list[/{page:\d+}]", ["as" => "vehicle.list", "uses" => "VehicleController@getList"]);
});


$router->group(['middleware' => "auth:admin", "prefix" => "insurance"], function() use ($router) {
    //create
    $router->post("add", "InsuranceController@create");
    $router->post("finalize", "InsuranceController@finalize");
    $router->post("view", ["as" => "insurance.view", "uses" => "InsuranceController@view"]);
    $router->post("list[/{page:\d+}]", ["as" => "insurance.list", "uses" => "InsuranceController@getList"]);

    $router->group(["prefix" => "amortisation"], function() use ($router) {
        //create
        $router->post("unpaid/list", "AmortisationController@getUnpaidList");
        $router->post("view", ["as" => "amortisation.view", "uses" => "AmortisationController@view"]);
    });


});

$router->group(['middleware' => "auth:admin", "prefix" => "payment"], function() use ($router) {
    //create
    $router->post("add", "PaymentController@create");
    $router->post("check/amortisation", "PaymentController@check");
});

$router->group(['middleware' => "auth:admin", "prefix" => "analytics"], function() use ($router) {
    //create
    $router->post("counts", "AnalyticsController@counts");
});