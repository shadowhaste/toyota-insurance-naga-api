<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 50)->unique();
            $table->unsignedInteger('customer_id');
            $table->string('customer_uuid', 50)->index();
            $table->string('model_no', 150)->unique();
            $table->string('make', 150)->index();
            $table->string('color', 150)->index();
            $table->string('cs_no', 150)->index();
            $table->string('vin', 150)->index();
            $table->string('vsi', 150)->index();
            $table->string('engine', 150)->index();
            $table->date('release_date')->index();
            $table->string('description', 255)->nullable();
            $table->string('displacement', 150)->index()->nullable();
            $table->integer('year_model')->nullable()->default(0);
            $table->decimal('gross_weight')->nullable();
            $table->integer('key_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
