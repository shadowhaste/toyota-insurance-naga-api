<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmortisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amortisations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 50)->index();
            $table->unsignedInteger('insurance_id');
            $table->string('insurance_uuid', 50)->index();
            $table->date('due_date')->nullable();
            $table->decimal('amount', 10, 2)->default(0);
            $table->boolean('is_paid')->default(0);
            $table->string('payment_reference', 150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amortisations');
    }
}
