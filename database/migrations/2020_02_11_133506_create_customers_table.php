<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            //personal
            $table->increments('id');
            $table->string('uuid', 50)->unique();
            $table->string('email', 130)->unique();
            $table->string('tin', 130)->nullable();
            $table->string('lastname', 150);
            $table->string('firstname', 150);
            $table->string('middlename', 150);
            $table->string('suffix', 32)->nullable();
            $table->string('address', 225)->nullable();
            $table->string('phone_number', 150)->nullable()->index();
            $table->string('mobile_number', 15)->index();
            $table->string('facebook_id', 255)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('gender', 10)->nullable();
            $table->string('civil_status', 32)->nullable();
            $table->string('height', 10)->nullable();
            $table->string('weight', 10)->nullable();

            //workplace
            $table->string('occupation', 225);
            $table->string('current_employer', 225)->nullable();
            $table->string('current_employer_address', 225)->nullable();
            $table->string('industry', 225)->nullable();
            $table->string('office_phone_number', 150)->nullable();
            $table->string('email_or_website', 150)->nullable();

            //family details
            $table->string('spouse_lastname', 150)->nullable();
            $table->string('spouse_firstname', 150)->nullable();
            $table->string('spouse_middlename', 150)->nullable();
            $table->string('spouse_suffix', 32)->nullable();
            $table->string('spouse_mobile_number', 15)->index()->nullable();
            $table->string('spouse_occupation', 225)->nullable();
            $table->string('spouse_current_employer', 225)->nullable();
            $table->string('spouse_current_employer_address', 225)->nullable();
            $table->string('spouse_industry', 225)->nullable();
            $table->string('spouse_office_phone_number', 150)->nullable();
            $table->string('spouse_email_or_website', 150)->nullable();
            $table->json('children')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
