<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 50)->unique();
            $table->string('reference_id', 150)->unique();
            $table->string('provider', 150);
            $table->unsignedInteger('vehicle_id');
            $table->string('vehicle_uuid', 50)->index();
            $table->date('registration_date')->nullable();
            $table->date('expiration_date')->nullable();;
            $table->date('reminder_date_whole')->nullable();
            $table->date('reminder_date_half')->nullable();
            $table->decimal('total_amount_issued', 10, 2)->nullable();
            $table->decimal('total_amount_with_aon', 10, 2)->nullable();
            $table->decimal('total_amount_without_aon', 10, 2)->nullable();
            $table->boolean('status')->default(0); //quote or final
            $table->boolean('is_expired')->default(0);
            $table->string('type', 150); //Comprehesive or CPTL
            $table->integer('payable_duration')->default(0); //payable duration in months
            $table->string('provision_no', 150)->nullable(); //trigger for free insurance
            $table->decimal('initial_payment', 10, 2)->default(0);
            $table->string('has_availed', 150)->nullable(); //aon or waon
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance');
    }
}
