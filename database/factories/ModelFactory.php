<?php

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(\App\Models\Admin::class, function (Faker\Generator $faker) {
    return [
        'uuid' => $faker->uuid,
        'username' => $faker->email,
        'password' => 'password',
    ];
});

$factory->define(\App\Models\Customer::class, function (Faker\Generator $faker) {
    return [
        'uuid' => $faker->uuid,
        'email' => $faker->email,
        'tin'=> rand(1000,9999),
        'lastname' => $faker->lastName,
        'firstname' => $faker->firstNameMale,
        'middlename' => $faker->word,
        'suffix' => null,
        'address' =>$faker->address,
        'phone_number' =>$faker->phoneNumber,
        'mobile_number' => '09565424447',
        'facebook_id' => $faker->url,
        'birth_date' => $faker->date(),
        'gender' =>$faker->randomElement(['M', 'F']),
        'civil_status' => $faker->randomElement(['Single', 'Married']),
        'height' => rand(1,100),
        'weight'=> rand(1,100),

        'occupation' => $faker->word,
        'current_employer' => $faker->word,
        'current_employer_address' => $faker->address,
        'industry' => $faker->word,
        'office_phone_number'=>$faker->phoneNumber,
        'email_or_website'=>$faker->email,

        'spouse_lastname' => $faker->lastName,
        'spouse_firstname' => $faker->firstNameMale,
        'spouse_middlename' => $faker->word,
        'spouse_suffix' => null,
        'spouse_mobile_number' => '09565424447',
        'spouse_occupation'  => $faker->word,
        'spouse_current_employer'  => $faker->word,
        'spouse_current_employer_address'  => $faker->address,
        'spouse_industry' => $faker->word,
        'spouse_office_phone_number'=>$faker->phoneNumber,
        'spouse_email_or_website'=>$faker->email,
        'children' => json_encode([
            0 => [
                "name" => $faker->name,
                "age" => $faker->randomElement([11, 12, 13]),
                "sex" => $faker->randomElement(['M', 'F']),
                "birthday" => $faker->date()
            ],
            1 => [
                "name" => $faker->name,
                "age" => $faker->randomElement([11, 12, 13]),
                "sex" => $faker->randomElement(['M', 'F']),
                "birthday" => $faker->date()
            ],

        ])
    ];
});


$factory->define(\App\Models\Vehicle::class, function (Faker\Generator $faker) {
    return [
        'uuid' => $faker->uuid,
        'customer_id' => null,
        'customer_uuid' => $faker->uuid,
        'model_no' => str_random(10),
        'make' => "Toyota",
        'description' => $faker->sentence,
        'color' => $faker->colorName,
        'cs_no' => str_random(10),
        'vin' => str_random(10),
        'vsi' => str_random(10),
        'engine'=> str_random(10),
        'displacement' => str_random(10),
        'year_model' => $faker->year,
        'gross_weight' => random_int(1000, 5000),
        'key_number' => random_int(1000, 5000),
    ];
});

$factory->define(\App\Models\Insurance::class, function (Faker\Generator $faker) {
    return [
        'uuid' => $faker->uuid,
        'provider' => $faker->company,
        'reference_id'=> str_random(10),
        'vehicle_id' => null,
        'vehicle_uuid' => null,
        'registration_date' => $faker->date(),
        'expiration_date' => $faker->date(),
        'type' => $faker->randomElement(["cptl", "comprehensive"])
    ];
});

$factory->define(\App\Models\InsuranceDetail::class, function (Faker\Generator $faker) {
    return [
        'uuid',
        'insurance_id' => null,
        'insurance_uuid' => null,
        'coverage' => $faker->word,
        'amount_issued'=> random_int(1000, 5000),
        'amount_with_aon'=> random_int(1000, 5000),
        'amount_without_aon' => random_int(1000, 5000),
    ];
});

$factory->define(\App\Models\Amortisation::class, function (Faker\Generator $faker) {
    return [
        'uuid' => $faker->uuid,
        'insurance_id' => null,
        'insurance_uuid'=> null,
        'due_date'=> \Carbon\Carbon::now()->format('Y-m-d'),
        'amount'=> random_int(1000, 5000),
    ];
});