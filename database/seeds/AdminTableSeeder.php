<?php

use Illuminate\Database\Seeder;
use App\Services\AdminService as Service;

class AdminTableSeeder extends Seeder {

    protected $service;

    public function __construct(Service $service) {
        $this->service = $service;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->service->create([
            "username" => "test",
            "password" => "test",
        ]);
    }

}
